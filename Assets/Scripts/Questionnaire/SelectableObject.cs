﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableObject : MonoBehaviour
{ // SelectaleObject is attached to child object of imagetarget that must also have collider for raycast
    // Index must match the one given to the mushroom in QuestionSource

    [SerializeField] private int objectIndex;

    public int ObjectIndex { get => objectIndex; }
}
