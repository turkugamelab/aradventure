﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    // ScoreManager is static singleton class that contains the score related values and methods

    public static ScoreManager instance = null;

    [SerializeField] private int score;
    [SerializeField] private int numberOfAnwered;

    private List<int> answeredQuestions;
    private bool answered;


    void Awake()
    {
        // ScoreManager singleton script
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        answeredQuestions = new List<int>();
        score = 0;
    }

    // Returns the current score
    public int Score { get => score; }

    // Returns the number of answered questions
    public int NumberOfAnwered { get => numberOfAnwered; }

    // Adds to score, adds the index of question to list of answered questions and updates the number of answered questions
    public void AddScore(int scoreChange, int questionIndex)
    {
        score += scoreChange;

        answeredQuestions.Add(questionIndex);

        numberOfAnwered = answeredQuestions.Count;
    }

    // Checks if the question has already been answered
    public bool HasThisBeenAnswered(int questionIndex)
    {
        if (answeredQuestions.Contains(questionIndex))
        {
            answered = true;
        }
        else
        {
            answered = false;
        }

        return answered;
    }
    
}
