﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionSource : MonoBehaviour
{ // QuestionSource includes all the values of a single quiz. Attached to imagetarget object
    // TODO: should be called when vuforia detects image target. For test use TEST button with objectPassTest script

    [SerializeField] private int questionIndex; // The unique index of the question
    [SerializeField] private bool isObjectQuiz; // Is this object quiz (AR object selection)? If not it's option quiz (multiple selection button). ONLY CHECK THIS OR OBJECTIVE!
    [SerializeField] private bool isObjectiveQuiz; // IS this quiz a series of real world objectives with no questions to answer? ONLY CHECK THIS OR OBJECT!
    [SerializeField] private string question; // Question shown to the player before answer is chosen
    [SerializeField] private string[] answers; // List of the answers available OR in case of objective quiz, these are displayed in order
    [SerializeField] private int correctAnswer; // Index of correct answer in the aswers array (NOT necessary for objective quiz)
    [SerializeField] private string explanation; // Info text shown to the player after answer is given


    public int QuestionIndex { get => questionIndex; }
    public bool IsObjectQuiz { get => isObjectQuiz; }
    public string Question { get => question; }
    public string[] Answers { get => answers; }
    public int CorrectAnswer { get => correctAnswer; }
    public string Explanation { get => explanation; }
    public bool IsObjectiveQuiz { get => isObjectiveQuiz; }
}
