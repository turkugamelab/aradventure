﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ObjectQuiz : MonoBehaviour
{ // The OptionQuiz class manages functions unique to AR object selecting quiz

    private SelectableObject selectedObject;
    private QuestionSource question;

    [SerializeField] private SelectableObject[] selectableObjects;

    private bool correct;

    // Texts used to show the currently selected object name to player
    [SerializeField] private Text uiName;
    [SerializeField] private Text uiText;


    void Start()
    {
        correct = false;
        uiName.text = "";
    }

    void Update()
    {

// Mouse controls, should also work directly in mobile

        // When left mouse button is pressed a ray (screen point) is cast
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            // Checks that pointer is not on top of gui element and that question is initialized
            if (Physics.Raycast(ray, out hit)  && !EventSystem.current.IsPointerOverGameObject(-1) && (question != null))
            {
                print("Raycast hit");
                // If the ray hits gameobject tagged "Mushroom" it will get values from the selected mushroom
                if (hit.transform.gameObject.CompareTag("Mushroom"))
                {
                    selectedObject = hit.transform.gameObject.GetComponent<SelectableObject>();
                    GetSelectedData();
                }
            }
        }

        // Touch controls, probably redundant.
        /*
#if UNITY_IOS || UNITY_ANDROID

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                print("Raycast hit");
                if (hit.transform.gameObject.CompareTag("Mushroom"))
                {
                    selectable = hit.transform.gameObject.GetComponent<SelectableObject>();
                    IsAnswerCorrect(selectable.Correct);
                    GetSelectableName(selectable.ObjectName);
                }
            }
        }
#endif
*/
    }

    public void OpenObjectQuiz(QuestionSource questionSource) // Called by manager, passes the current question
    {
        question = questionSource;
    }

    private void GetSelectedData() // gets the index and name of the selected object and displays it
    {
        uiText.text = "Valittu:";
        uiName.text = question.Answers[selectedObject.ObjectIndex];

        if (selectedObject.ObjectIndex == question.CorrectAnswer)
        {
            correct = true;
        }
        else
        {
            correct = false;
        }
    }

    public void AnswerQuestion() // Confirms player's currently selected answer and calls quiz manager
    {
        GetComponentInParent<QuestionnaireManager>().ShowResult(correct, selectedObject.ObjectIndex);
    }

}
