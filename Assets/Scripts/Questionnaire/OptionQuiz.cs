﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionQuiz : MonoBehaviour
{ // The OptionQuiz class manages functions unique to multiple choise button questionnaires

    // Answer buttons must be added to the list in editor
    [SerializeField] private Button[] optionButtons;
    [SerializeField] private Text questionText;
    [SerializeField] private Button playButton;

    private QuestionSource question;
    private bool correct;
    private AudioSource sound;
    
    public void OpenOptionsQuiz(QuestionSource questionSource)
    { // Called by manager. Passes the current question and assigns the Answers arrays strings to buttons
        question = questionSource;

        for (int i = 0; i < question.Answers.Length; i++)
        {
            optionButtons[i].GetComponentInChildren<Text>().text = question.Answers[i];
        }

        questionText.text = question.Question;

        if (questionSource.GetComponent<AudioSource>() != null)
        {
            sound = question.GetComponent<AudioSource>();
            playButton.gameObject.SetActive(true);
        }
        else
        {
            playButton.gameObject.SetActive(false);
        }
    }

    public void PlayAudio()
    {
        sound.Play();
    }

    public void IsOptionCorrect(int answerIndex)
    { // When a button is pressed it calls this with its index value and this calls the manager with the answer
        //Index must be assigned manually in editor and must match index in the Buttons array

        if (answerIndex == question.CorrectAnswer)
        {
            correct = true;
            print("Correct button");
        }
        else
        {
            correct = false;
            print("Wrong button");
        }

        sound = null;

        GetComponentInParent<QuestionnaireManager>().ShowResult(correct, answerIndex); // Calls manager with the results
    }
    
}
