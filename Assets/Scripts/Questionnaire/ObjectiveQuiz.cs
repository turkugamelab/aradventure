﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveQuiz : MonoBehaviour
{
    private QuestionSource question;
    [SerializeField] private Text objectiveText;
    private int currentObjective;


    public void OpenObjectiveQuiz(QuestionSource questionSource)
    {
        question = questionSource;

        objectiveText.text = question.Answers[0];

        currentObjective = 1;
    }

    public void NextObjective()
    {
        if (currentObjective < question.Answers.Length)
        {
            objectiveText.text = question.Answers[currentObjective++];
        }
        else
        {
            GetComponentInParent<QuestionnaireManager>().FinishObjective();
        }
    }
}
