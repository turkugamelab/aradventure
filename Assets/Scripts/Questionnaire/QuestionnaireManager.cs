﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionnaireManager : MonoBehaviour
{ // Manages questionnaire function

    private QuestionSource question;

    [SerializeField] private Canvas objectQuizCanvas; // Canvas that includes AR object selection quiz elemets
    [SerializeField] private Canvas optionQuizCanvas; // Canvas that includes multiple choise button quiz elements
    [SerializeField] private Canvas objectiveQuizCanvas; // Canvas that includes real world objective "quiz" related elements

    [SerializeField] private Canvas resultCanvas; // Canvas shown after player answers question
    [SerializeField] private Canvas questionCanvas; // Canvas that shows the question to the player

    [SerializeField] private Text resultTitle; // Title text shown in result canvas (tells if player answered right of wrong)
    [SerializeField] private Text resultText; // Information text shown to the player after anwering

    private string answer;

    private ScoreManager scoreManager;

    public CurrencyManager currencyManager;

    void Start()
    {
        scoreManager = ScoreManager.instance;

        objectQuizCanvas.enabled = false;
        optionQuizCanvas.enabled = false;
        objectiveQuizCanvas.enabled = false;
        resultCanvas.enabled = false;
        questionCanvas.enabled = false;
    }

    public void InitializeQuiz(QuestionSource questionSource)
        // TODO: Called when vuforia detects ImageTarget (QuestionSource is a component of specific ImageTarget)
        // Calls either object- or option-quiz and sets question and explanation texts
        // Currently can be called with the test script button
    {
        if (scoreManager.HasThisBeenAnswered(questionSource.QuestionIndex) == false)
        {
            question = questionSource;

            questionCanvas.enabled = true;
            questionCanvas.GetComponentInChildren<Text>().text = question.Question;
            resultText.text = question.Explanation;


            if (question.IsObjectQuiz) // For AR object quiz
            {
                objectQuizCanvas.enabled = true;
                objectQuizCanvas.GetComponent<ObjectQuiz>().OpenObjectQuiz(question);
            }
            else if (question.IsObjectiveQuiz) // For objective "quiz"
            {
                objectiveQuizCanvas.enabled = true;
                objectiveQuizCanvas.GetComponent<ObjectiveQuiz>().OpenObjectiveQuiz(question);
            }
            else // For button options quiz
            {
                questionCanvas.enabled = false;
                optionQuizCanvas.enabled = true;
                optionQuizCanvas.GetComponent<OptionQuiz>().OpenOptionsQuiz(question);
            }
        }
        else { print("Question has already been answered"); }
    }

    public void ShowResult(bool correct, int answerIndex)
        // Called with answer-button from object-quiz or the option buttons from option-quiz
        // Closes Quiz canvas, adds fitting title text and opens the result page.
        // Alters score and lists answered question
    {
        answer = question.Answers[answerIndex];
        questionCanvas.enabled = true;
        optionQuizCanvas.enabled = false;
        objectQuizCanvas.enabled = false;
        resultCanvas.enabled = true;

        if (correct)
        {
            scoreManager.AddScore(1, question.QuestionIndex);

            resultTitle.text = answer + " on oikea vastaus!";
            print("Add to score, lock question");

            currencyManager.AddCurrency(100);
        }
        else
        {
            scoreManager.AddScore(0, question.QuestionIndex);

            resultTitle.text = answer + " on väärä vastaus...\nOikea vastaus on " + question.Answers[question.CorrectAnswer] + ".";
            print("No score, lock question");
        }

    }

    public void FinishObjective()
        // Used instead of ShowResult for objective tasks. Automatically gives a point.
    {
        objectiveQuizCanvas.enabled = false;
        resultCanvas.enabled = true;

        resultTitle.text = "Tehtävä suoritettu!";
        scoreManager.AddScore(1, question.QuestionIndex);
        currencyManager.AddCurrency(100);
    }

        public void CloseQuestionnaire() // Called from close button, closes all quiz canvases
        {
        objectQuizCanvas.enabled = false;
        optionQuizCanvas.enabled = false;
        resultCanvas.enabled = false;
        questionCanvas.enabled = false;
        objectiveQuizCanvas.enabled = false;
        }
}
