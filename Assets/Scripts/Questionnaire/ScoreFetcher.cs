﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreFetcher : MonoBehaviour
{
    // Score fetching script for debugging score (better solution should be made)

    private ScoreManager scoreManager;
    [SerializeField] private Text scoreText;

    private void Start()
    {
        scoreManager = ScoreManager.instance;
    }
    // Update is called once per frame
    void Update()
    {
        if (Time.frameCount % 10 == 0)
        {
            scoreText.text = scoreManager.Score + "/" + scoreManager.NumberOfAnwered;
        }
    }
}
