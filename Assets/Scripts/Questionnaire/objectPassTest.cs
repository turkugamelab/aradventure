﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectPassTest : MonoBehaviour
{ // Test function for calling the manager with a questionsource
    [SerializeField] private QuestionSource questionSource;
    [SerializeField] private QuestionnaireManager manager;

    public void SendToManager()
    {
        manager.InitializeQuiz(questionSource);
    }
}
