﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizManager : MonoBehaviour
{

    public GameObject Panel1;
    public GameObject Panel2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OpenQuiz(){
        Debug.Log("Quiz Openned");

        if(Panel1 != null || Panel2 != null)
        {
            Panel1.SetActive(true);
            Panel2.SetActive(true);
        }
    }

    public void CloseQuiz()
    {
        Debug.Log("Quiz Closed");

        if (Panel1 != null || Panel2 != null)
        {
            Panel1.SetActive(false);
            Panel2.SetActive(false);
        }
    }



}
