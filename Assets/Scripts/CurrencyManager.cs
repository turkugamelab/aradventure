﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CurrencyManager : MonoBehaviour
{

    public int currentCredits;
    public TextMeshProUGUI currencyTextField;

    // Start is called before the first frame update
    void Start()
    {
        currentCredits = PlayerPrefs.GetInt("Currency", 0);
        updateCurrencyUI();
    }

    public void AddCurrency(int amount)
    {
        currentCredits = currentCredits + amount;
        PlayerPrefs.SetInt("Currency", currentCredits);
        updateCurrencyUI();
    }

    public void SubstractCurrency(int amount)
    {
        currentCredits = currentCredits - amount;
        if(currentCredits < 0)
        {
            currentCredits = 0;
        }
        PlayerPrefs.SetInt("Currency", currentCredits);
        updateCurrencyUI();
    }

    private void updateCurrencyUI()
    {
        if (currencyTextField != null)
        {
            currencyTextField.text = "Rahat: "+currentCredits.ToString()+"c";
        }
    }

    public void ResetCurrency()
    {

        currentCredits = 0;

        PlayerPrefs.SetInt("Currency", currentCredits);
        updateCurrencyUI();
    }

}
