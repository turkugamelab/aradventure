﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Location;
using Mapbox.Unity.Map;
using System;

public class Player : MonoBehaviour
{

    Animator animator;
    Rigidbody rb;
    public Transform target;
    public float moveSpeed = 1.0f;
    public float rotationSpeed = 1.0f;

    public Vector3 offset;
    public float smoothSpeed = 0.125f;

    Vector3 prevPos;
    Vector3 newPos;
    Vector3 velocity;
    Vector3 prevVelocity;

    public GameObject[] AvailableCharacters;

    private int CurrentCharacter;

    void Start()
    {
        SpawnPlayer();

        

        

        prevPos = transform.position;
        newPos = transform.position;

    }

    void FixedUpdate()
    {

        MovePlayer();
        
    }

    private void SpawnPlayer()
    {
        CurrentCharacter = PlayerPrefs.GetInt("Character", 0);

        GameObject Character = Instantiate(AvailableCharacters[CurrentCharacter], this.gameObject.transform);

        animator = Character.GetComponent<Animator>();
        rb = Character.GetComponent<Rigidbody>();
    }

    private void MovePlayer()
    {
        newPos = transform.position;  // each frame track the new position
        velocity = (newPos - prevPos) / Time.fixedDeltaTime;  // velocity = dist/time
        prevPos = newPos;  // update position for next frame calculation

        // Smoothed movement
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;

        // Smooth turning
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position), rotationSpeed * Time.deltaTime);

        // Get & smooth velocity for animations
        velocity = Vector3.Lerp(prevVelocity, velocity, 0.1f);
        prevVelocity = velocity;

        // Apply velocity in animator
        animator.SetFloat("Speed", velocity.magnitude * 0.33f);

    }

}
