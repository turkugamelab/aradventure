﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QUitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }

    public void ComeBackMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void Questionnaire()
    {
        SceneManager.LoadScene(3);
    }
}

