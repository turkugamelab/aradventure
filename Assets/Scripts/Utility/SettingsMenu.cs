﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

[RequireComponent(typeof(Slider))]
public class SettingsMenu : MonoBehaviour
{
    private Slider mySlider;
    //public AudioSource volumeAudio;
    //public AudioMixer audioMixer;
    //public GameObject volumeAudio;

    private void Start()
    {
        mySlider = GetComponent<Slider>();

        mySlider.value = AudioManager.instance.menuMusic.volume;

        mySlider.onValueChanged.AddListener(delegate { OnValueChanged(); });
    }

    public void OnValueChanged()
    {
        AudioManager.instance.menuMusic.volume = mySlider.value;
    }
}
