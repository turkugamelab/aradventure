﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterSelectionManager : MonoBehaviour
{

    public CurrencyManager currencyManager;

    public Transform selectableCharacterParent;
    public GameObject[] characters;

    public int[] defaultCharacters = new int[] { 0, 1 };
    public List<int> availableCharacters;

    public TextMeshProUGUI characterName;
    public int characterCost = 100;

    public int currentCharacter;
    public int selectedCharacter;

    public GameObject buyButton;

    // Start is called before the first frame update
    void Start()
    {
        buyButton.SetActive(false);

        currentCharacter = PlayerPrefs.GetInt("Character", 0);
        selectedCharacter = currentCharacter;

        availableCharacters = new List<int>(PlayerPrefsX.GetIntArray("UnlockedCharacters"));

        foreach (int character in defaultCharacters)
        {
            if (!availableCharacters.Exists(element => element == character))
            {
                availableCharacters.Add(character);
            }
        }
        
        SpawnCharacter();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeCharacter(bool prev)
    {
        if(selectedCharacter == characters.Length - 1)
        {
            selectedCharacter = 0;
            SpawnCharacter();

        } else
        {

            selectedCharacter = selectedCharacter + 1;
            SpawnCharacter();
        }
        

    }

    private void SpawnCharacter()
    {

        if (selectableCharacterParent.childCount != 0)
        {
            Destroy(selectableCharacterParent.GetChild(0).gameObject);
        }

        Instantiate(characters[selectedCharacter], selectableCharacterParent);
        characterName.text = characters[selectedCharacter].name.ToUpper();

        if(availableCharacters.Exists(element => element == selectedCharacter))
        {
            // Do if character is unlocked
            characterName.fontStyle = FontStyles.Normal;
            currentCharacter = selectedCharacter;
            PlayerPrefs.SetInt("Character", currentCharacter);
            buyButton.SetActive(false);
            
        }
        else
        {
            // Do if not unlocked
            characterName.fontStyle = FontStyles.Strikethrough;
            buyButton.SetActive(true);
        }
        
    }

    public void BuyCharacter()
    {

        

        if(currencyManager.currentCredits >= characterCost)
        {
            availableCharacters.Add(selectedCharacter);
            PlayerPrefsX.SetIntArray("UnlockedCharacters", availableCharacters.ToArray());
            currencyManager.SubstractCurrency(100);

            characterName.fontStyle = FontStyles.Normal;
            currentCharacter = selectedCharacter;
            PlayerPrefs.SetInt("Character", currentCharacter);
            buyButton.SetActive(false);
        }

        
    }

    public void ResetCharacters()
    {
        availableCharacters = new List<int>(defaultCharacters);
        PlayerPrefsX.SetIntArray("UnlockedCharacters", availableCharacters.ToArray());

        selectedCharacter = 0;
        currentCharacter = selectedCharacter;
        PlayerPrefs.SetInt("Character", currentCharacter);

        SpawnCharacter();
    }
}
