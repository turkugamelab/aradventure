﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance = null;

    [SerializeField] private GameObject blackScreen;
    [SerializeField] [Range(0.1f, 2f)] private float fadeInSpeed = 1f;
    [SerializeField] [Range(0.1f, 2f)] private float fadeOutSpeed = 1f;
    [SerializeField] [Range(0f, 3f)] private float blackScreenTime = 1f;

    [SerializeField] private GameObject blackScreenParent;

    private Image playerBlackScreen;
    public Image PlayerBlackScreen { get { return playerBlackScreen; } }

    private Coroutine blackScreenFadeCoroutine;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        InstantiateBlackScreen();
        PlayerSpawnFadeOut();

    }

    //Instantiates a black sprite as the child of the main camera to the distance of the near clip plane
    void InstantiateBlackScreen()
    {
        GameObject blackScreenCopy = GameObject.Instantiate(blackScreen, blackScreenParent.transform);
        playerBlackScreen = blackScreenCopy.GetComponent<Image>();

        
    }

    public void ChangeToSceneByName(string sceneName)
    {
        //Checks if there is already a fade in progress, if there is it will stop that fade and start a new one
        if (blackScreenFadeCoroutine == null)
            blackScreenFadeCoroutine = StartCoroutine(FadeToScene(sceneName));
        else
        {
            StopCoroutine(blackScreenFadeCoroutine);
            blackScreenFadeCoroutine = StartCoroutine(FadeToScene(sceneName));
        }
    }

    //Called when the Player is spawned to make sure the black screen is instantiated under the player's camera
    void PlayerSpawnFadeOut()
    {
        //Checks if there is already a fade in progress, if there is it will stop that fade and start a new one
        if (blackScreenFadeCoroutine == null)
            blackScreenFadeCoroutine = StartCoroutine(FadeFromBlack(instance.PlayerBlackScreen));
        else
        {
            StopCoroutine(blackScreenFadeCoroutine);
            blackScreenFadeCoroutine = StartCoroutine(FadeFromBlack(instance.PlayerBlackScreen));
        }
    }

    //Fades to a new scene once the screen has turned black
    IEnumerator FadeToScene(string sceneName)
    {
        yield return StartCoroutine(FadeToBlack(instance.PlayerBlackScreen));
        yield return new WaitForSeconds(blackScreenTime);

        SceneManager.LoadScene(sceneName);
    }

    IEnumerator FadeToBlack(Image fadingSprite)
    {
        if (fadingSprite != null)
        {
            for (float i = fadingSprite.color.a; i < 1f; i += Time.deltaTime * fadeInSpeed)
            {
                fadingSprite.color = new Color(fadingSprite.color.r, fadingSprite.color.g, fadingSprite.color.b, i);
                yield return new WaitForSeconds(0.01f);
            }
            fadingSprite.color = new Color(fadingSprite.color.r, fadingSprite.color.g, fadingSprite.color.b, 1f);
        }
    }

    IEnumerator FadeFromBlack(Image fadingSprite)
    {
        if (fadingSprite != null)
        {
            for (float i = fadingSprite.color.a; i > 0f; i -= Time.deltaTime * fadeOutSpeed)
            {
                fadingSprite.color = new Color(fadingSprite.color.r, fadingSprite.color.g, fadingSprite.color.b, i);
                yield return new WaitForSeconds(0.01f);
            }
            fadingSprite.color = new Color(fadingSprite.color.r, fadingSprite.color.g, fadingSprite.color.b, 0f);
        }
    }
}
