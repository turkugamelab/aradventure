﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Examples;

public class MapManager : MonoBehaviour
{

    public AstronautMouseController astronautMouseController;
    public CharacterMovement characterMovement;
    public Transform CharacterParent;
    public GameObject[] AvailableCharacters;

    private int CurrentCharacter;

    // Start is called before the first frame update
    void Start()
    {
        SpawnPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SpawnPlayer()
    {

        CurrentCharacter = PlayerPrefs.GetInt("Character", 0);

        if (CharacterParent.childCount != 0)
        {
            Destroy(CharacterParent.GetChild(0).gameObject);
        }

        GameObject newCharacter = Instantiate(AvailableCharacters[CurrentCharacter], CharacterParent);

        characterMovement.CharacterAnimator = newCharacter.GetComponent<Animator>();

        astronautMouseController.Character = newCharacter;
        astronautMouseController.CharacterAnimator = newCharacter.GetComponent<Animator>();
    }
}
