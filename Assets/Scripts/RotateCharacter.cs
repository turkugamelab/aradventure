﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCharacter : MonoBehaviour
{

    public float RotateSpeed = 0.2f;

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(new Vector3(0, RotateSpeed, 0));
    }
}
