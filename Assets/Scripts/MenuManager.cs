﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{

    public CurrencyManager currencyManager;

    public int[] defaultCharacters = new int[] { 0 };
    private List<int> availableCharacters;

    public void ResetCharacters(){
        availableCharacters = new List<int>(defaultCharacters);
        PlayerPrefsX.SetIntArray("UnlockedCharacters", availableCharacters.ToArray());

        int currentCharacter = 0;
        PlayerPrefs.SetInt("Character", currentCharacter);

        currencyManager.ResetCurrency();
    }
}
